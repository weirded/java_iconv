package ru.weirded.ustu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author oleg
 */
public class Encoding {
    public static String outputOf(String[] cmdline) {
        String output = "";
        try {
            String s;
            for (int i = 0; i < cmdline.length; i++)
            	System.out.print(cmdline[i] + " ");
            System.out.println();
            Process p = Runtime.getRuntime().exec(cmdline);
            p.waitFor();
            InputStreamReader input = new InputStreamReader(p.getInputStream());
            InputStreamReader error = new InputStreamReader(p.getErrorStream());
            BufferedReader br = new BufferedReader(input);
            try {
                while ((s = br.readLine()) != null) {
                    output+= s + '\n';
                }
                br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
            
            br = new BufferedReader(error);
            try {
                while ((s = br.readLine()) != null) {
                    output+= '\n' + s;
                }
                br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            return null;
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return output;
    }

    public static String convert(File f, String from, String to) {
    	File log = new File("/var/log/jconv.log");
        String[] cmd = { "iconv", "-f", from, "-t", to, f.getAbsolutePath() };
        String output = outputOf(cmd);
    	FileWriter fw;
		try {
			fw = new FileWriter(log, true);
			fw.append(from + " " + to + "\n");
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
        return output;
    }

    public static String convert(String fileName, String to) {
    	if (fileName == null)
    		return "Error, no such file";
    	String from;
    	File log = new File("/var/log/jconv.log");
    	File f = new File(fileName);
    	from = enca(f);
        String[] cmd = { "iconv", "-f", from, "-t", to, f.getAbsolutePath() };
        String output = outputOf(cmd);
    	FileWriter fw;
		try {
			fw = new FileWriter(log, true);
			fw.append(from + " " + to + "\n");
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
        return output;
    }
    
    public static String enca(File f) {
    	String[] cmd = { "enca", f.getAbsolutePath() };
    	String output = outputOf(cmd);
    	if (output.contains("1251"))
        	return "cp1251";
        if (output.contains("8-r"))
        	return "koi8-r";
        if (output.contains("UTF-8") || output.contains("ASCII"))
        	return "utf-8";
        return "unknown";
    }
}
