/**
 *
 */
package ru.weirded.ustu;

/**
 * @author Интерфейс для перекодирования файлов
 */
import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.layout.*;
import java.util.*;
import java.io.*;

public class SWTUI {
	static String fileName;
	static FileWriter fOut;
	static FileReader fIn;
	static Image images[];

	static Display display;
	static Shell shell;
	static Text errText, originalText, decodingText;
	static Label l2, lErr;
	static Menu bar, subFile, subConv, subFun;
	static MenuItem fileItem, funItem, convItem, itemOpen, itemSave, itemExit,
			convUTF8, convCP1251, convKOI8R, itemRevise, itemTable, itemGraph,
			itemHelp;
	static TabFolder tabFolder;
	static TabItem tabOriginal, tabDecoding, tabGraphic, tabErr;
	static Group grpGr, grpOriginal, grpDecoding, grpErr;
	static Color black, white, red;
	static GC gc;
	
	static double minX, maxX, minY, maxY, scaleX, scaleY, dx, dy;
	static boolean isError = false, isVerify = false, isTable = false,
			isGraph = false;
	static double[] A, Xi, Yi;
	static int colX, penW = 1, penG = 2, w, h, x0, y0, koi8 = 0, utf8 = 0,
			cp1251 = 0, bigText = SWT.BORDER | SWT.MULTI | SWT.V_SCROLL;

	static private void doSave() {
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		fileName = dialog.open();
		if (fileName == null)
			return;
		try {
			fOut = new FileWriter(fileName);
			fOut.write(decodingText.getText());
			fOut.close();
		} catch (IOException eio) {
			setError("Ошибка при записи в файл " + fileName + "\n");
		}
	}

	static private void countDecoders() {
		koi8 = 0;
		utf8 = 0;
		cp1251 = 0;
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("/var/log/jconv.log"));
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				if (line.length() > 0) {
					if (line.matches(".*cp1251"))
						cp1251++;
					if (line.matches(".*koi8-r"))
						koi8++;
					if (line.matches(".*utf-8"))
						utf8++;
				}
				doEval();
			}
		} catch (FileNotFoundException e) {
			utf8 = 4;
			koi8 = 5;
			cp1251 = 13;
			doEval();
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static private int evalMin() {
		int min = utf8;
		if (koi8 < utf8)
			min = koi8;
		if (cp1251 < koi8)
			min = cp1251;
		return min;
	}

	static private int evalMax() {
		int max = utf8;
		if (koi8 > utf8)
			max = koi8;
		if (cp1251 > koi8)
			max = cp1251;
		return max;
	}

	static Listener toUTF8 = new Listener() {
		public void handleEvent(Event e) {
			decodingText.setText(Encoding.convert(fileName, "utf-8"));
		}
	};

	static Listener toCP1251 = new Listener() {
		public void handleEvent(Event e) {
			decodingText.setText(Encoding.convert(fileName, "cp1251"));
		}
	};

	static Listener toKOI8 = new Listener() {
		public void handleEvent(Event e) {
			decodingText.setText(Encoding.convert(fileName, "koi8-r"));
		}
	};

	static Listener onSave = new Listener() {
		public void handleEvent(Event e) {
			doSave();
		}
	};

	private static void doOpen() {
		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		fileName = dialog.open();
		System.out.println(fileName);
		if (fileName == null)
			return;
		try {
			fIn = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fIn);
			String line;
			originalText.setText("");
			while ((line = br.readLine()) != null)
				if (line.length() > 0)
					originalText.append(line + "\n");
			fIn.close();
		} catch (IOException eio) {
			setError("Ошибка при чтении из файла " + fileName + "\n");
		}
	}

	static Listener onOpen = new Listener() {
		public void handleEvent(Event e) {
			doOpen();
		}
	};

	static private void setError(String s) {
		errText.append(s);
		isError = true;
	}

	static private void doRevise() {
		isError = false;
		minX = 0;
		maxX = 3;
		if (isError) {
			tabErr.setText("Неполадки (1)");
			isVerify = false;
		} else {
			tabErr.setText("Неполадки");
			isVerify = true;
		}
		isTable = false;
		isGraph = false;
	}

	static Listener onRevise = new Listener() {
		public void handleEvent(Event e) {
			doRevise();
		}
	};

	static private void doEval() {
		colX = 4;
		Xi = new double[colX];
		Yi = new double[colX];
		Xi[0] = 0;
		Yi[0] = 0;
		Xi[1] = 1;
		Yi[1] = utf8;
		Xi[2] = 2;
		Yi[2] = cp1251;
		Xi[3] = 3;
		Yi[3] = koi8;
		isTable = true;
		isGraph = true;
	}

	static Listener onTable = new Listener() {
		public void handleEvent(Event e) {
			doRevise();
			doEval();
		}
	};

	static private GC graphPrepare(Event e) {
		if (e.type != 9)
			gc = new GC(grpGr);
		else
			gc = (GC) e.gc;
		black = display.getSystemColor(SWT.COLOR_BLACK);
		white = display.getSystemColor(SWT.COLOR_WHITE);
		red = display.getSystemColor(SWT.COLOR_RED);
		gc.setBackground(white);
		gc.setForeground(black);
		gc.fillRectangle(grpGr.getBounds());
		gc.setLineWidth(penW);
		return gc;
	}

	static private void graphDataPrepare() {
		countDecoders();
		minY = evalMin();
		maxY = evalMax();
		for (int i = 1; i < colX; i++) {
			if (minY > Yi[i])
				minY = Yi[i];
			if (maxY < Yi[i])
				maxY = Yi[i];
		}
		w = grpGr.getBounds().width - 10;
		h = grpGr.getBounds().height - 15;
		scaleX = w / ((maxX - minX) * 1.10);
		scaleY = h / ((maxY - minY) * 1.10);
		dx = (maxX - minX) / 5.0;
		dy = (maxY - minY) / 5.0;
		x0 = (int) Math.floor((0 - minX) * scaleX);
		y0 = (int) Math.floor(h - (0 - minY) * scaleY);
	}

	static private void drawLegend() {
		gc.drawText("Статистика конвертирования", 150, 30);
		gc.drawText("Кодировка", w - 20, h - 10);
		gc.drawText("Частота использования", 5, 15);
	}

	static private void drawLines() {
		for (int i = 0; i < 6; i++) {
			double x = Math.floor(minX + dx * i);
			double y = Math.floor(minY + dy * i);
			int x1 = (int) Math.floor((x - minX) * scaleX);
			int y1 = (int) Math.floor(h - (y - minY) * scaleY);
			if (i == 2)
				gc.drawText("utf8", x1, h + 1);
			if (i == 4)
				gc.drawText("cp1251", x1, h + 1);
			if (i == 5)
				gc.drawText("koi8-r", x1, h + 1);
			gc.drawLine(x1, 30, x1, h);
			gc.drawText(new Formatter().format("%.2f", y).toString(), 3,
					y1 - 20);
			gc.drawLine(0, y1, w, y1);
		}
	}

	static private void drawGraph() {
		x0 = (int) Math.floor((Xi[0] - minX) * scaleX);
		y0 = (int) Math.floor(h - (Yi[0] - minY) * scaleY);
		gc.setForeground(red);
		gc.setLineWidth(4);
		for (int i = 1; i < colX; i++) {
			int x1 = (int) Math.floor((Xi[i] - minX) * scaleX);
			int y1 = (int) Math.floor(h - (Yi[i] - minY) * scaleY);
			gc.drawLine(x1, y0, x1, y1);
			x0 = x1;
		}
	}

	static private void doGraph(Event e) {
		if ((isGraph) & (e.type == 9)) {
			gc = graphPrepare(e);
			graphDataPrepare();
			drawLegend();
			drawLines();
			drawGraph();
			if (e.type == 13) {
				gc.dispose();
			}
		}
	}

	static Listener onGraph = new Listener() {
		public void handleEvent(Event e) {
			doGraph(e);
		}
	};

	private static void prepareShell() {
		display = new Display();
		shell = new Shell(display);
		shell.setLayout(new FillLayout());
		shell.setText("Конвертер кодировок");
		shell.setSize(700, 600);
		shell.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {

				if (images != null) {
					for (int i = 0; i < images.length; i++) {
						final Image image = images[i];
						if (image != null)
							image.dispose();
					}
					images = null;
				}
			}
		});
	}

	private static void menuItemHelper(MenuItem mi, Menu parent, Menu m,
			String text) {
		mi = new MenuItem(parent, SWT.CASCADE);
		mi.setText(text);
		mi.setMenu(m);
	}

	private static void menuSubItemHelper(MenuItem mi, Menu submenu, String s) {
		mi = new MenuItem(submenu, SWT.PUSH);
		mi.setText(s);
	}

	private static void menuSubItemHelper(MenuItem mi, Menu sub, String s,
			Listener l) {
		mi = new MenuItem(sub, SWT.PUSH);
		mi.setText(s);
		mi.addListener(SWT.Selection, l);
	}

	private static void prepareMenu() {
		bar = new Menu(shell, SWT.BAR);
		subFile = new Menu(shell, SWT.DROP_DOWN);
		subConv = new Menu(shell, SWT.DROP_DOWN);
		subFun = new Menu(shell, SWT.DROP_DOWN);
		menuItemHelper(fileItem, bar, subFile, "Файлы");
		menuItemHelper(convItem, bar, subConv, "Перекодировать");
		menuItemHelper(funItem, bar, subFun, "Функции");
		menuItemHelper(itemHelp, bar, null, "Справка");
		menuSubItemHelper(itemOpen, subFile, "Открыть", onOpen);
		menuSubItemHelper(itemSave, subFile, "Сохранить", onSave);
		menuSubItemHelper(itemExit, subFile, "Выход");
		menuSubItemHelper(convUTF8, subConv, "В UTF-8", toUTF8);
		menuSubItemHelper(convCP1251, subConv, "В CP1251", toCP1251);
		menuSubItemHelper(convKOI8R, subConv, "В Koi8-r", toKOI8);
		menuSubItemHelper(itemRevise, subFun, "Проверить задание", onRevise);
		menuSubItemHelper(itemTable, subFun, "Построить график", onTable);
		menuSubItemHelper(itemGraph, subFun, "Напечатать график");
		shell.setMenuBar(bar);
	}

	static private void prepareTabOriginal() {
		grpOriginal = new Group(tabFolder, SWT.SHADOW_IN);
		originalText = new Text(grpOriginal, bigText);
		originalText.setLocation(5, 35);
		originalText.setSize(550, 470);
		tabOriginal = new TabItem(tabFolder, SWT.NONE);
		tabOriginal.setText("Оригинальный файл");
		tabOriginal.setControl(grpOriginal);
	}

	static private void prepareTabDecoding() {
		grpDecoding = new Group(tabFolder, SWT.SHADOW_IN);
		decodingText = new Text(grpDecoding, bigText);
		decodingText.setLocation(5, 35);
		decodingText.setSize(550, 470);
		tabDecoding = new TabItem(tabFolder, SWT.NONE);
		tabDecoding.setText("Результат конвертирования");
		tabDecoding.setControl(grpDecoding);
	}

	static private void prepareTabGraph() {
		grpGr = new Group(tabFolder, SWT.SHADOW_IN);
		grpGr.addListener(SWT.Paint, onGraph);
		tabGraphic = new TabItem(tabFolder, SWT.NONE);
		tabGraphic.setText("График");
		tabGraphic.setControl(grpGr);
	}

	static private void prepareTabError() {
		grpErr = new Group(tabFolder, SWT.SHADOW_IN);
		lErr = new Label(grpErr, SWT.LEFT);
		lErr.setText("Сообщения об ошибках");
		lErr.setLocation(5, 15);
		lErr.setSize(145, 20);
		errText = new Text(grpErr, bigText);
		errText.setLocation(5, 35);
		errText.setSize(550, 470);
		tabErr = new TabItem(tabFolder, SWT.NONE);
		tabErr.setText("Неполадки");
		tabErr.setControl(grpErr);
	}

	static private void prepareTabs() {
		tabFolder = new TabFolder(shell, SWT.BORDER);
		prepareTabOriginal();
		prepareTabDecoding();
		prepareTabGraph();
		prepareTabError();
	}

	private static void prepare() {
		prepareShell();
		prepareMenu();
		prepareTabs();
	}

	private static void show() {
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}

	public static void main(String[] args) {
		prepare();
		show();
	}
}
